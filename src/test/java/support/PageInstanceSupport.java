package support;


import pageObjects.*;

public class PageInstanceSupport {

//    PICS
    public Page page = new Page();
    public DashboardPage dashboardPage = new DashboardPage();
    public LoginPage loginPage = new LoginPage();
    public InsureGuardPage insureGuardPage = new InsureGuardPage();
    public RegistrationPage registrationPage = new RegistrationPage();
    public TradesPage tradesPage = new TradesPage();

}
