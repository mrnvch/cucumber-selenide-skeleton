package pageObjects;


import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Configuration.SelectorMode.Sizzle;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.*;

public class Page {

    public Page() {
        System.setProperty("webdriver.ie.driver", "drivers/chromedriver");
        System.setProperty("phantomjs.binary.path", "drivers/phantomjs/bin/phantomjs");
        if (conf.getString("browser").equals("ch")) {
            System.setProperty("browser", "chrome");
        }

    }

    public static final Logger LOGGER = Logger.getLogger(Page.class.getName());
    protected Config conf = ConfigFactory.load();
    public String path = System.getProperty("user.dir");
    public File fileForUpload = new File(path + "/TestDoc.pdf");
    public String fileForUploadWindows = "C:\\Users\\PICSQA\\Desktop\\TestDoc.pdf";
    public String os = (String) executeJavaScript("return navigator.userAgent;");
    private  Long unique = System.currentTimeMillis();

    public static String lastGeneratedCompanyName;
    public static String lastGeneratedUser;


    /* LOCATORS */

    public SelenideElement infoMessage() {
        return $(By.xpath("//div[@class='info']"));
    }
    public SelenideElement userMenuLinkPics() {
        return $("a#user_menu");
    }
    public SelenideElement logoutPics() {
        return $("#logout");
    }
    public SelenideElement searchTermPics() {
        return $(By.name("searchTerm"));
    }
    public SelenideElement scrollToElement(SelenideElement element){
        if (conf.getString("browser").equals("ch")) {
            executeJavaScript("window.scroll(" + (element.getLocation().getX()) + "," + (element.getLocation().getY() - 90) + ");");
        }
        return element;
    }
    public SelenideElement btn(String btn){
        if($(By.xpath("//input[@value='"+btn+"']")).exists()){
            return $(By.xpath("//input[@value='"+btn+"']"));
        } else if($(By.xpath("//button[@value='"+btn+"']")).exists()){
            return $(By.xpath("//button[@value='"+btn+"']"));
        }else{
            return $(By.xpath("//button[@type='"+btn+"']"));
        }
    }
    public SelenideElement accountSwitched() {
        return $("#user_menu .account>span");
    }
    public SelenideElement savingAnswerWait() {
        return $(".blockUI.blockMsg.blockElement").waitUntil(disappear, 5000);
    }
    public SelenideElement verifyingWait() {
        return $(".blockUI").waitUntil(disappear, 5000);
    }
    public SelenideElement loadingWait() {
        return $(".blockUI").waitUntil(disappear, 5000);
    }


    //  Abstracted Locator finders
    public SelenideElement inputFieldAudit(String questionNumber){
        return $("li:contains('" + questionNumber + "'):first input[type='text']");
    }
    public SelenideElement textAreaAudit(String questionNumber){
        return $("li:contains('" + questionNumber + "'):first textarea");
    }
    public SelenideElement checkboxAudit(String questionNumber){
        return $("li:contains('" + questionNumber + "'):first input[type='checkbox']");
    }
    public SelenideElement radioAudit(String questionNumber, String value) {
        return $("li:contains('" + questionNumber + "'):first input[value='" + value + "']");
    }
    public SelenideElement uploadFileBtn(String questionNumber) {
        return $("li:contains('" + questionNumber + "'):first input[type='button']");
    }

    public void uploadNewFileInsurance(String questionNumber) {
        $("li:contains('" + questionNumber + "'):first a.add.uploadNewCertificate").waitUntil(visible, 5000).click();

        switchToWindow(1);

        if(os.contains("Windows")){
            $("#fileTextbox").waitUntil(visible, 5000).sendKeys(fileForUploadWindows);
        }
        else {
            $("#fileTextbox").waitUntil(visible, 5000).uploadFile(fileForUpload);
        }

        $(".picsbutton.positive").waitUntil(visible, 5000).click();

        switchToWindow(0);
        savingAnswerWait();
    }

    public SelenideElement attachExistingFileInsurance(String questionNumber) {
        return $("li:contains('" + questionNumber + "'):first a.showExistingCertificates").waitUntil(visible, 5000);
    }
    public SelenideElement selectAudit(String questionNumber){
        return $("li:contains('" + questionNumber + "'):first select");
    }
    public SelenideElement auditCategory(String categoryName) {
        return $("#auditHeaderSideNav a:contains('" + categoryName + "')");
    }


    /* METHODS */

    public void openPicsUrl() {
        if(getGrid()!= null){
            WebDriver driver = new RemoteWebDriver(getSeleniumRemoteServerURL(), getCapabilities());
            //WebDriver driver = new RemoteWebDriver(server, capabilities);
            WebDriverRunner.setWebDriver(driver);
        }

        if(WebDriverRunner.isIE()){
            clearBrowserCache();
        }

        if (conf.getString("browser").equals("ch")) {
            getWebDriver().manage().window().setSize(new Dimension(1024,768));
        }

        if (!url().contains("/Login.action")){
            if(url().contains("/Report.action")){
                close();
            }
            getWebDriver().manage().deleteAllCookies();
            clearBrowserCache();
            open("https://".concat(getEnv()).concat(".picsorganizer.com"));
            if (conf.getString("browser").equals("ch")) {
                getWebDriver().manage().window().setSize(new Dimension(1024,768));
            }
        }
        Configuration.selectorMode = Sizzle;
    }

    public void openIgrUrl() {
        if(getGrid()!= null){
            WebDriver driver = new RemoteWebDriver(getSeleniumRemoteServerURL(), getCapabilities());
            //WebDriver driver = new RemoteWebDriver(server, capabilities);
            WebDriverRunner.setWebDriver(driver);
        }

        if(WebDriverRunner.isIE()){
            clearBrowserCache();
        }

        if (conf.getString("browser").equals("ch")) {
            getWebDriver().manage().window().setSize(new Dimension(1024,768));
        }

        if (!url().contains("/login")){
            getWebDriver().manage().deleteAllCookies();
            clearBrowserCache();
            open("http://" + conf.getString(conf.getString("env").concat(".urlIgr")));
            if (conf.getString("browser").equals("ch")) {
                getWebDriver().manage().window().setSize(new Dimension(1024,768));
            }
        }
        Configuration.selectorMode = Sizzle;
    }


    public void userLogoutPics() {
        userMenuLinkPics().click();
        logoutPics().click();
    }

    public void clickLink(String linkName){
        $(By.linkText(linkName)).click();
    }

    public void searchForCompanyPics(String name) {
        searchTermPics().val(name);
        sleep(1500);
        $("#user_searchbox .clearfix:first div:contains('" + name + "')").click();
    }

    public void clickBtn(String btnName){
        if(btnName.equals("Synchronize")){
            scrollToElement(btn(btnName));
            btn(btnName).click();
            sleep(7000);
            infoMessage().shouldHave("Successfully refreshed");
        }
        else {
            btn(btnName).scrollTo().click();
        }
    }

    public void linkShouldBeVisible(String textLink) {
        $(By.linkText(textLink)).shouldBe(visible);
    }
    public void linkShouldNotBeVisible(String textLink) {
        $(By.linkText(textLink)).shouldNotBe(visible);
    }

    public void timeOut() {
        sleep(conf.getInt(conf.getString("env").concat(".timeout")));
    }

    public void fileUploadInsurancePics() {
        switchToWindow(1);
        if(os.contains("Windows")){
            if(WebDriverRunner.isIE()){
                $(By.id("fileTextbox")).waitUntil(visible, 8000).sendKeys(fileForUploadWindows);
            }else {
                $("#fileTextbox").waitUntil(visible, 8000).sendKeys(fileForUploadWindows);
            }
        }else {
            $("#fileTextbox").waitUntil(visible, 8000).uploadFile(fileForUpload);
        }
        $(".picsbutton.positive").waitUntil(visible, 8000).click();
        switchToWindow(0);
        sleep(1000);
    }

    public String generateCompanyName(){
        String companyName = "My Company" + unique;
        lastGeneratedCompanyName = companyName;
        return companyName;
    }
    public String generateUser(){
        String userName = "qa" + unique;
        lastGeneratedUser = userName;
        return userName;
    }



    public String getEnv(){
        String getbaseUrl;
        String environment;
        getbaseUrl = System.getProperty("base.url");
        if (getbaseUrl == null) {
            environment =  conf.getString("env");
        } else {
            environment = getbaseUrl;
        }
        return environment;
    }
    public String getPassword(){
        String password;
        if (getEnv() == null) {
            password = conf.getString(conf.getString("env").concat(".password"));
        }else {
            password = conf.getString(getEnv().concat(".password"));
        }
        return password;
    }
    public String getGrid() {
        String grid = System.getProperty("run");
        return grid;
    }
    private static URL getSeleniumRemoteServerURL() {
        String seleniumRemoteServerIP = System.getProperty("server");
        String domainAddress = (seleniumRemoteServerIP == null || seleniumRemoteServerIP.length() == 0)
                ? "localhost" : seleniumRemoteServerIP;

        try {
            return new URL("http://" + domainAddress + ":4444/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
    private static DesiredCapabilities getCapabilities() {
        Proxy proxy = new Proxy();
        proxy.setAutodetect(false);
        proxy.setProxyType(Proxy.ProxyType.DIRECT);

//        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(CapabilityType.PROXY, proxy);

        String browser = System.getProperty("browser");

        if (browser != null) {
            if (browser.equals("internetexplorer")) {
                capabilities = DesiredCapabilities.internetExplorer();
            }
            if (browser.equals("chrome")) {
                capabilities = DesiredCapabilities.chrome();
            }
        }

        return capabilities;
    }
    public String getCurrentDate(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
