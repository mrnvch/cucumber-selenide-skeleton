package pageObjects;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by ca00456-mb on 2/27/15.
 */
public class LoginPage extends Page{
    public SelenideElement loginUsernamePics() {
        return $("#username");
    }

    public SelenideElement loginPasswdPics() {
        return $("#password");
    }

    public SelenideElement loginBtnPics() {
        return $("button.btn.btn-primary");
    }
    public void clickRegistrationLink() {
        $(By.linkText("Are you a contractor not currently registered with PICS?")).click();
    }

    public void userLoginPics(String username, String password) {
        loginUsernamePics().val(username);
        loginPasswdPics().sendKeys(password);
        loginBtnPics().click();
    }
}
