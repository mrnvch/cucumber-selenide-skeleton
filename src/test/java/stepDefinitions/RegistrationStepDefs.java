package stepDefinitions;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import support.PageInstanceSupport;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.sleep;


public class RegistrationStepDefs extends PageInstanceSupport {

    protected Config conf = ConfigFactory.load();



    @When("^User click on Registration Link$")
    public void User_click_on_Registration_Link() throws Throwable {
        loginPage.clickRegistrationLink();
        sleep(500);
    }

    @When("^Contractor completes registration section$")
    public void Contractor_completes_registration_section() throws Throwable {

//        Long unique = System.currentTimeMillis();
        String language = "English";
        String dialect = "United States";
        String country = "United States";
        String timeZone = "America/Boise";
        String legalName = page.generateCompanyName();
        String address = "123 Main St";
        String city = "Irvine";
        String state = "California";
        String zip = "92618";
        String firstName = "John";
        String lastName = "Doe";
        String email = "picsqateam@test.com";
        String phone = "111-222-3333";
        String username = page.generateUser();
        String password = conf.getString(conf.getString("env").concat(".password"));

        registrationPage.registerNewUserInfo(language, dialect, country, timeZone, legalName, address, city,
                state, zip, firstName, lastName, email, phone, username, password);

        try {
            String filename = "CreatedUsers.txt";
            FileWriter fw = new FileWriter(filename, true); //the true will append the new data
            fw.write("Company Name: " + legalName + "  " + "username: " + username + " \n");//appends the string to the file
            fw.close();
        } catch (IOException ioe) {
            System.err.println("IOException: " + ioe.getMessage());
        }
    }

    @And("^User adds Client Sites:$")
    public void User_adds_Client_Sites(DataTable table) throws Throwable {
        List<List<String>> data = table.raw();

        for (int i = 0; i < data.size(); i++) {
            for (int j = 0; j < 1; j++) {
                registrationPage.addClientSite(data.get(i).get(j));
                page.timeOut();
            }
        }

        User_should_see_following_Client_Sites_in_the_Selected_client_sites_section(table);
    }

    @Then("^(?:User|Contractor) should see following Client Sites in the Selected client sites section:$")
    public void User_should_see_following_Client_Sites_in_the_Selected_client_sites_section(DataTable table) throws Throwable {
        List<List<String>> data = table.raw();
        ElementsCollection clientSites = $$(By.xpath(".//*[@id='RegistrationAddClientSite']/section/div[2]/div/ul/li/a/span[1]"));

        List<SelenideElement> appSiteNames = new ArrayList<SelenideElement>();
        List<String> tableSiteNames = new ArrayList<String>();


        for (SelenideElement site : clientSites) {
            appSiteNames.add(site);
        }

        for (int i = 0; i < data.size(); i++) {
            for (int j = 0; j < 1; j++) {
                String tableSiteName = data.get(i).get(j);
                tableSiteNames.add(tableSiteName);
            }
        }

        page.timeOut();
        Collections.reverse(tableSiteNames);

        for (int k = 0; k < data.size()-1; k++) {
            SelenideElement appSiteName = appSiteNames.get(k);
            String tableSiteName = tableSiteNames.get(k);
            appSiteName.shouldHave(exactText(tableSiteName));
        }
    }
    @And("^User selects service \"([^\"]*)\"$")
    public void User_selects_service(String servicePerformed) throws Throwable {
        switch (servicePerformed) {
            case "Onsite Services":
                registrationPage.onSite();
                break;
            case "Offsite Services":
                registrationPage.offSite();
                break;
            case "Material Supplier":
                registrationPage.materialSupplier("Yes", "Yes");
                break;
            case "Transportation Services":
                registrationPage.transportation("Yes", "Yes", "Yes", "Yes");
        }

    }

    @When("^(?:User|Contractor) makes payment$")
    public void User_makes_payment() throws Throwable {
        page.timeOut();
        registrationPage.iAgreeCheckBox();
        registrationPage.SubmitPaymentButton();
        page.timeOut();
        dashboardPage.takeCompanyName();
    }

    @Given("^Contractor completes Registration with following client site\\(s\\):$")
    public void Contractor_completes_Registration_with_following_client_site_s_(DataTable table) throws Throwable {
        CommonStepDefs commonStepDefs = new CommonStepDefs();
        commonStepDefs.Contractor_navigates_to_login_page();
        User_click_on_Registration_Link();
        Contractor_completes_registration_section();
        User_adds_Client_Sites(table);
        page.clickBtn("Save & Next");
        User_selects_service("Onsite Services");
        User_selects_service("Offsite Services");
        User_selects_service("Material Supplier");
        User_selects_service("Transportation Services");
        registrationPage.enterNumberOfEmployees("5");
        registrationPage.safetyServiceEvaluation("Yes", "Yes", "Yes", "Yes", "Yes", "Yes", "Yes", "Yes");
        registrationPage.productSafetyEvaluation("High");
        page.clickBtn("Save & Next");

        User_makes_payment();
    }
}
