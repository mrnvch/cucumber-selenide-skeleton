package stepDefinitions;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import support.PageInstanceSupport;

import java.util.logging.Logger;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class CommonStepDefs extends PageInstanceSupport {

    private static final Logger LOGGER = Logger.getLogger(PageInstanceSupport.class.getName());

    @Given("^(?:Contractor) navigates to login page$")
    public void Contractor_navigates_to_login_page() throws Throwable {
        page.openPicsUrl();
        LOGGER.info("Open URL: https://".concat(page.getEnv()).concat(".picsorganizer.com"));
    }

    @And("^(?:User|CSR|Contractor) goes to \"([^\"]*)\" -> \"([^\"]*)\"$")
    public void User_goes_to_(String pageName1, String pageName2) throws Throwable {
        $(By.linkText(pageName1)).waitUntil(visible, 10000).click();
        sleep(500);
        $(By.partialLinkText(pageName2)).waitUntil(visible, 10000).click();
    }

    @Then("^(?:User|CSR|Contractor|Auditor) Logout$")
    public void User_Logout() throws Throwable {
        page.userLogoutPics();
        LOGGER.info("User Logout");
        page.timeOut();
    }

}
