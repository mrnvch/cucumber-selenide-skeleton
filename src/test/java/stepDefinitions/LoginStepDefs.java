package stepDefinitions;

import cucumber.api.java.en.When;
import support.PageInstanceSupport;

import static com.codeborne.selenide.Selenide.sleep;

/**
 * Created by ca00456-mb on 2/27/15.
 */
public class LoginStepDefs extends PageInstanceSupport {

    @When("^Contractor clicks on Registration Link$")
    public void Contractor_clicks_on_Registration_Link() throws Throwable {
        loginPage.clickRegistrationLink();
        sleep(500);
    }

}
