package stepDefinitions;

import cucumber.api.java.en.And;
import org.openqa.selenium.By;
import support.PageInstanceSupport;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class TradesStepDefs extends PageInstanceSupport {

    @And("^(?:User|Contractor) Add new Trade \"([^\"]*)\"$")
    public void User_Add_new_Trade(String tradeName) throws Throwable {
        tradesPage.searchForTrade(tradeName);
        $(By.linkText(tradeName)).waitUntil(visible, 8000).click();
        if(tradesPage.addButton().waitUntil(visible, 6000).exists()){
            tradesPage.addButton().click();
        } else {
            tradesPage.saveButton().click();
        }
    }

}
