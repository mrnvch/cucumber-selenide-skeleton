package pageObjects;


import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.util.logging.Logger;

import static com.codeborne.selenide.Selenide.$;

public class DashboardPage {

    public String companyTitle;
    public static final Logger LOGGER = Logger.getLogger(Page.class.getName());



    public SelenideElement companyName(){ return $(By.xpath("//*[@id='ContractorView__page']/h1"));}


    public void takeCompanyName(){
        String companyName = companyName().getText().substring(0,23);
        LOGGER.info("Created Company Name: "+companyName);
        this.companyTitle = companyName;
    }
}
