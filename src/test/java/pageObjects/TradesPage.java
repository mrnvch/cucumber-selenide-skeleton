package pageObjects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by ca00456-mb on 2/27/15.
 */
public class TradesPage {

    /* Locators */
    public SelenideElement searchTrades() {return $(".searchText");}
    public SelenideElement searchButton() {return $(".searchButton");}
    public SelenideElement saveButton(){ return $(".trade-form__saveTradeAjax");}
    public SelenideElement removeButton(){ return $(".trade-form__removeTradeAjax");}
    public SelenideElement activityPercentSelect(){ return $(By.id("trade-form_trade_activityPercent"));}
    public SelenideElement tradeOptionSelect(){ return $(By.id("trade-form_trade_selfPerformed"));}
    public SelenideElement activityPercentVerify(){ return $(By.xpath("//*[@id='activityPercent']/p/b"));}
    public SelenideElement tradeOptionVerify(){ return $(By.xpath("//*[@id='tradeOptions']/p/b"));}
    public SelenideElement addButton(){ return $(By.id("addButton"));}


    public void searchForTrade(String value) {
        searchTrades().waitUntil(Condition.visible, 10000).clear();
        searchTrades().val(value);
        searchButton().click();
    }
}
