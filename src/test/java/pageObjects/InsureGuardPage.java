package pageObjects;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by ca00456-mb on 2/27/15.
 */
public class InsureGuardPage extends Page {

//    Audit Header
    public SelenideElement idAuditHeader() {
        return $("#auditHeader fieldset li:contains('ID')");
    }
    public SelenideElement expiresAuditHeader() {
        return $("#auditHeader fieldset li:contains('Expires')");
    }
    public SelenideElement safetyProfessionalAuditHeader() {
        return $("#auditHeader fieldset li:contains('Safety Professional'):last");
    }
    public SelenideElement safetyManualAuditHeader() {
        return $("#auditHeader fieldset li:contains('Safety Manual')");
    }
    public SelenideElement closingSafetyProfessionalAuditHeader() {
        return $("#auditHeader fieldset li:contains('Closing Safety Professional')");
    }
    public SelenideElement operatorScopeAuditHeader(String site) {
        return $("#auditHeader td[title='" + site + "']");
    }
    public SelenideElement progressPercentAuditHeader(){ return $(".progressPercent");}
    public SelenideElement statusAuditHeader(){
       return $(".caoStatus.hoverable");
    }
    public SelenideElement statusSelectAuditHeader(){ return $("#caoTableForm_caosSave_0__status");}
    public SelenideElement penIconAuditHeader(){ return $(By.xpath("//span[@class='right']/a[@class='edit']"));}
    public SelenideElement dateAuditHeader() {
        return $(".caoDate");
    }
    public SelenideElement submitButtonAuditHeader(){ return $(".singleButton.aqua.button");}
    public SelenideElement naButtonAuditHeader() {
        return $(".singleButton.gray.button");
    }
    public SelenideElement completeButtonAuditHeader(){ return $(".singleButton.green.button");}
    public SelenideElement rejectButtonAuditHeader(){ return $(".policy-reject.red.button");}

//    Audit Body
    public SelenideElement doneButton(){ return $("#done");}

}
