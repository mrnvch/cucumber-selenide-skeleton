package pageObjects;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

/**
 * Created by ca00456-mb on 2/27/15.
 */
public class RegistrationPage extends Page{

    public SelenideElement searchClientSite() {
        return $(By.id("RegistrationAddClientSiteFilter_searchValue"));
    }


    private void selectClientSite(String siteName) {
        $("li:contains('" + siteName + "') a").scrollTo().click();
    }
    public void iAgreeCheckBox() {
        $(By.xpath("//*[@id='contractor_agreement']/input")).click();
    }
    public void SubmitPaymentButton() {
        $(By.id("submit_payment_button")).click();
    }



//    Registration Page
    public void registerNewUserInfo(String language, String dialect, String country, String timeZone, String legalName, String address,
                                    String city,String state, String zip, String firstName,
                                    String lastName, String email, String phone,
                                    String username, String password){

        $(By.xpath(".//*[@id='s2id_registration_language']/a/span[2]/b")).waitUntil(visible, 8000).click();
        $("#select2-drop li:contains('" + language + "')").click();
        $(By.xpath(".//*[@id='s2id_dialect_selection']/a/span[2]/b")).waitUntil(visible, 8000).click();
        $("#select2-drop li:contains('" + dialect + "')").click();
        $(By.xpath(".//*[@id='s2id_Registration_registrationForm_countryISOCode']/a/span[2]/b")).waitUntil(visible, 8000).click();
        $("#select2-drop li:contains('" + country + "')").click();
        $(By.xpath("//*[@id='s2id_contractor_timezone']/a/span[2]/b")).waitUntil(visible, 8000).click();
        $(By.xpath("//*[@id='select2-drop']/ul/li[3]/div/article/p[1]/span[contains(text(),'" + timeZone + "')]")).click();
        $(By.id("Registration_registrationForm_legalName")).val(legalName);
        $(By.id("Registration_registrationForm_address")).val(address);
        $(By.id("Registration_registrationForm_city")).val(city);
        $(By.xpath("//*[@id='s2id_Registration_contractor_countrySubdivision']/a/span[2]/b")).click();
        $(By.xpath("//*[@id='select2-drop']/ul/li/div[contains(text(),'"+state+"')]")).click();
        $(By.id("Registration_registrationForm_zip")).val(zip);
        $(By.id("Registration_registrationForm_firstName")).val(firstName);
        $(By.id("Registration_registrationForm_lastName")).val(lastName);
        $(By.id("contact_email")).val(email);
        $(By.id("Registration_registrationForm_phone")).val(phone);
        $(By.id("account_username")).clear();
        $(By.id("account_username")).val(username);
        $(By.id("Registration_registrationForm_password")).val(password);
        $(By.id("Registration_registrationForm_passwordConfirmation")).val(password);
        $(By.id("Registration_button_GetStarted")).click();
    }

//    Add Client Site Page
    public void addClientSite(String clientSite) {
        searchClientSite().val(clientSite).pressEnter();
        sleep(2000);
        selectClientSite(clientSite);
        sleep(2000);
    }

//    Service Evaluation Page
    public void onSite() {
    $(By.id("onSite")).click();
}
    public void offSite() {
        $(By.id("offSite")).click();
    }
    public void materialSupplier(String question1YesOrNo, String question2YesOrNo) {
        $("#materialSupplier").click();
        sleep(1000);
        selectRadio(By.name("answerMap[7660].answer"), question1YesOrNo);
        selectRadio(By.name("answerMap[7661].answer"), question2YesOrNo);
    }
    public void transportation(String question1YesOrNo, String question2YesOrNo, String question3YesOrNo, String question4YesOrNo) {
        $("#transportation").click();
        selectRadio(By.name("answerMap[14924].answer"), question1YesOrNo);
        selectRadio(By.name("answerMap[14925].answer"), question2YesOrNo);
        selectRadio(By.name("answerMap[14926].answer"), question3YesOrNo);
        selectRadio(By.name("answerMap[14927].answer"), question4YesOrNo);
    }

    public void safetyServiceEvaluation(String question1YesOrNo, String question2YesOrNo, String question3YesOrNo,
                                        String question4YesOrNo, String question5YesOrNo, String question6YesOrNo,
                                        String question7YesOrNo, String question8YesOrNo){

        selectRadio(By.name("answerMap[25524].answer"), question1YesOrNo);
        selectRadio(By.name("answerMap[25525].answer"), question2YesOrNo);
        selectRadio(By.name("answerMap[25526].answer"), question3YesOrNo);
        selectRadio(By.name("answerMap[25527].answer"), question4YesOrNo);
        selectRadio(By.name("answerMap[12342].answer"), question5YesOrNo);
        selectRadio(By.name("answerMap[12344].answer"), question6YesOrNo);
        selectRadio(By.name("answerMap[25528].answer"), question7YesOrNo);
        selectRadio(By.name("answerMap[12346].answer"), question8YesOrNo);

    }

    public void productSafetyEvaluation(String highORmediumORlow){
        selectRadio(By.name("answerMap[7679].answer"), highORmediumORlow);
    }

    public void enterNumberOfEmployees(String numEmployee) {
        $(By.id("RegistrationServiceEvaluation_registrationForm_numberOfEmployees")).val(numEmployee);
    }

//    Make Payment Page

}
