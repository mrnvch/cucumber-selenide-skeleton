Feature: Validation OCR

  @TC177
  Scenario: TC177: "Integration TC" Verifying the workflow if IGR User "Reject" submitted insurance by Contractor
    Given Contractor completes Registration with following client site(s):
      | Tesoro Anacortes Refinery           |
    When  User goes to "Company" -> "Trades"
    And User Add new Trade "Solar Electric Power Generation"
    And Contractor goes to "InsureGUARD" -> "General Liability (New)"
    And Contractor completes General Liability Insurance for "Tesoro Anacortes Refinery"
    And Contractor Submits General Liability Policy
    And Contractor Logout

    When IGR login
    And IGR searches and selects Contractor on the IGR Dashboard
    Then IGR can view the same document uploaded by Contractor
