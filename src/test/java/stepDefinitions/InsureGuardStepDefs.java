package stepDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import support.PageInstanceSupport;

import static com.codeborne.selenide.Condition.exactText;
import static java.lang.Thread.sleep;

/**
 * Created by ca00456-mb on 2/27/15.
 */
public class InsureGuardStepDefs extends PageInstanceSupport {

    @Then("^Contractor completes General Liability Insurance for \"([^\"]*)\"$")
    public void Contractor_completes_General_Liability_Insurance(String clientSite) throws Throwable {
        String limit = "10000000";

        page.inputFieldAudit("1.1.1").val("2015-01-19");
        page.savingAnswerWait();
        page.inputFieldAudit("1.1.2").val("2017-01-19");
        page.savingAnswerWait();
        page.inputFieldAudit("1.1.3").val("Super Insurance");

        //Policy Limits
        page.savingAnswerWait();
        page.inputFieldAudit("1.2.1").val(limit);
        page.savingAnswerWait();
        page.inputFieldAudit("1.2.2").val(limit);
        page.savingAnswerWait();
        page.inputFieldAudit("1.2.3").val(limit);
        page.savingAnswerWait();
        page.inputFieldAudit("1.2.4").val(limit);
        page.savingAnswerWait();
        page.inputFieldAudit("1.2.5").val(limit);
        page.savingAnswerWait();
        page.inputFieldAudit("1.2.6").val(limit);

        //TODO Add logic for Site Specific insurance !!!
        //Tesoro Refining & Marketing
        page.savingAnswerWait();
        switch (clientSite){
            case "Tesoro Anacortes Refinery":
                page.uploadNewFileInsurance("1.312.1");
                page.radioAudit("1.312.2", "Yes").click();
                break;
        }

        page.savingAnswerWait();
        page.scrollToElement(insureGuardPage.doneButton());
        insureGuardPage.doneButton().click();

    }

    @And("^Contractor Submits General Liability Policy$")
    public void Contractor_Submits_General_Liability_Policy() throws Throwable {
        int i = 0;
        while(!insureGuardPage.statusAuditHeader().has(exactText("Submitted")) && i < 3){
            insureGuardPage.submitButtonAuditHeader().click();
            sleep(3000);
            i++;
        }
    }
}
